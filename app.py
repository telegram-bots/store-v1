import telebot
import config
from texts import *
from keyboards import *

bot = telebot.TeleBot(config.token)

# ПРИ НАЖАТИИ КОМАНДЫ /START
@bot.message_handler(commands=['start'])
def cmd_start(message):
	# Отправляем основное сообщение и главное меню
	bot.send_message(message.chat.id, main_text,parse_mode="Markdown",reply_markup=main_menu)

# ПРИЕМ СООБЩЕНИЙ
@bot.message_handler(func=lambda message: True)
def add_position_name(message):
	# Удаляем любые входящие сообщения
	bot.delete_message(message.chat.id, message.message_id)

# РАБОТА С КОЛЛБЭКАМИ
@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
	bot.answer_callback_query(call.id, call.data, True)

bot.polling()	




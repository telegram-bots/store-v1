from telebot import types
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

main_menu = types.InlineKeyboardMarkup(row_width=3)
btn1 = types.InlineKeyboardButton(text="🗃 Категории", callback_data="categories")
btn2 = types.InlineKeyboardButton(text="📦 Позиции", callback_data="positions")
btn3 = types.InlineKeyboardButton(text="🎭 Юзеры", callback_data="users")
btn4 = types.InlineKeyboardButton(text="💳 Платежка", callback_data="wallet")
btn5 = types.InlineKeyboardButton(text="⚙️ Другие настройки", callback_data="settings")
main_menu.add(btn1,btn2,btn3,btn4,btn5)